#version

cmake_minimum_required( VERSION 2.8.1 )
 # name of the project
 project (ListInterger)

#version
set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 0)
#find Qt
find_package( Qt5Quick REQUIRED )

INCLUDE_DIRECTORIES( ${Qt5Quick_INCLUDE_DIRS} )
#include

#set compiler flags
#set( CMAKE_CXX_COMPILER "usr/bin/clang++")

 set( CMAKE_CXX_FLAGS "-Wall --std=c++11" )

#headers
set( HDRS


)

#SOURCES

set(SRCS

 main.cpp

)

#add_executable
add_executable(${PROJECT_NAME} ${SRCS} ${HDRS} ${RSRCS})

#target_link_libraries( ${PROJECT_NAME} Qt5::Quick)
#set_target_properties( ${PROJECT_NAME} PROPERTIES POSITION_INDEPENDENT_CODE ON)
